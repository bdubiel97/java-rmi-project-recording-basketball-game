package PackageServer;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class sServer {
	
	Registry reg;
	sServant servant;
	
	public static void main(String[] args) {
		try {
			new sServer();
		}	catch (Exception e) {
				e.printStackTrace(); 
				System.exit(1);
			} 
	}
	
	protected sServer() throws RemoteException {
		try { 
			reg = LocateRegistry.createRegistry(1099); 
			servant = new sServant();     
			reg.rebind("Server", servant);
			System.out.println("Server is READY");
		}	catch(RemoteException e) {
				e.printStackTrace(); 
				throw e;
			}
	}
}