package PackageServer;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import PackageInterfejsy.iCallback;
import PackageInterfejsy.iChat;
import instruction1.GameNumber;
import instruction1.GameDetails;
import instruction1.GameStats;
import instruction1.QuarterDetails;

public class sServant extends UnicastRemoteObject implements iChat{
	
	private Map<String, iCallback> people = new HashMap<String, iCallback>();
	private Map<String, GameDetails> GameDetailsMap = new HashMap<String, GameDetails>();
	private Map<String, QuarterDetails> QuarterDetailsMap = new HashMap<String, QuarterDetails>();
	private Map<String, GameStats> GameStatsMap = new HashMap<String, GameStats>();
	
	public sServant() throws RemoteException {	
	}
	
	// Implements register() method of Interface
	public boolean register(String nick, iCallback n) throws RemoteException {
		
		System.out.println("Server.register(): " + nick);
		if (!people.containsKey(nick)) {
			people.put(nick, n);
			return true;
		}
		return false;
	}
	
	// Implements unregister() method of Interface
	public boolean unregister(String nick) throws RemoteException {
		
		if (people.remove(nick) != null) {
			System.out.println("Server.unregister(): " + nick);
			return true;
		}
		return false;
	}
		

	
	// Implements saveGD() method of Interface
	public boolean saveGD(GameDetails packet) throws RemoteException {
			
		System.out.println("Server.saveGD(): " + packet.Competition);
		if (packet.Competition != null) {
			String key = packet.Competition + packet.GameNr;
			GameDetailsMap.put(key, packet);
			return true;
		}
		return false;
	}
	
	// Implements saveQD() method of Interface
	public boolean saveQD(QuarterDetails packet) throws RemoteException {
				
		System.out.println("Server.saveQD(): " + packet.Quarter);
		if (packet.Quarter != null) {
			String key = packet.Quarter + packet.GameNr;
			QuarterDetailsMap.put(key, packet);
			return true;
		}
		return false;
	}
	
	// Implements saveGS() method of Interface
	public boolean saveGS(GameStats packet) throws RemoteException {
				
			System.out.println("Server.saveGS(): " + packet.WhoWon);
			if (packet.WhoWon != null) {
				String key = packet.WhoWon + packet.GameNr;
				GameStatsMap.put(key, packet);
				return true;
			}
			return false;
		}
	

	
	// Implements mapGD() method of Interface
	public Vector<String> mapGD(String GameNr) throws RemoteException {
			
			System.out.println("Server.mapGD(): " + GameNr);
			Set<String> set = GameDetailsMap.keySet();
			Vector<String> v = new Vector<String>();
			for (String s : set)
				if(s.matches(GameNr))
					v.add(s);
			return v;
		}
		
	// Implements mapQD() method of Interface
	public Vector<String> mapQD(String GameNr) throws RemoteException {
					
					System.out.println("Server.mapQD(): " + GameNr);
					Set<String> set = QuarterDetailsMap.keySet();
					Vector<String> v = new Vector<String>();
					for (String s : set)
						if(s.matches(GameNr))
							v.add(s);
					return v;
				}

	// Implements mapGS() method of Interface
	public Vector<String> mapGS(String GameNr) throws RemoteException {
					
					System.out.println("Server.mapGS(): " + GameNr);
					Set<String> set = GameStatsMap.keySet();
					Vector<String> v = new Vector<String>();
					for (String s : set)
						if(s.matches(GameNr))
							v.add(s);
					return v;
				}
	

	
	
	// Implements showGD() method of Interface
	public Vector<GameDetails> showGD(String GameNr) throws RemoteException {
			
			System.out.println("Server.showGD(): " + GameNr);
			Set<String> set = GameDetailsMap.keySet();
			Vector<GameDetails> v = new Vector<GameDetails>();
			for (String s : set)
				if(s.contains(GameNr))
					v.add(GameDetailsMap.get(s));
			return v;
		}
		
	// Implements showQD() method of Interface
	public Vector<QuarterDetails> showQD(String GameNr) throws RemoteException {
					
					System.out.println("Server.showQD(): " + GameNr);
					Set<String> set = QuarterDetailsMap.keySet();
					Vector<QuarterDetails> v = new Vector<QuarterDetails>();
					for (String s : set)
						if(s.contains(GameNr))
							v.add(QuarterDetailsMap.get(s));
					return v;
				}
	
	// Implements showGS() method of Interface
	public Vector<GameStats> showGS(String GameNr) throws RemoteException {
				
				System.out.println("Server.showGS(): " + GameNr);
				Set<String> set = GameStatsMap.keySet();
				Vector<GameStats> v = new Vector<GameStats>();
				for (String s : set)
					if(s.contains(GameNr))
						v.add(GameStatsMap.get(s));
				return v;
			}
		

}	
