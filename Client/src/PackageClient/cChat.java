package PackageClient;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Vector;

import PackageInterfejsy.iCallback;
import PackageInterfejsy.iChat;

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import instruction1.GameDetails;
import instruction1.GameStats;
import instruction1.QuarterDetails;

public class cChat {
	
	private Scanner userInput = new Scanner(System.in);
	String username;
	iChat remoteObject;
	iCallback callback;
	public static void main(String[] args) throws FileNotFoundException {
		
		if(args.length < 1) {
			System.out.println("Usage: Client <server host name>");
			System.exit(-1);
		}
		new cChat(args[0]);
	}

	public cChat(String hostname) throws FileNotFoundException {
		
		System.out.println("Enter Login: ");
		if (userInput.hasNextLine())
			username = userInput.nextLine();
		Registry reg;
		try {			
			reg = LocateRegistry.getRegistry(hostname);			
			remoteObject = (iChat) reg.lookup("Server");
			callback = new cCallback();
			remoteObject.register(username, callback);
			loop();
			remoteObject.unregister(username);
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
	}
	
	
    void saveGD() {
		
		GameDetails packet = new GameDetails (0,0, username, username, username, username, username);
		System.out.println("Enter the data below ");
		System.out.println("Game Number: ");
		if (userInput.hasNextLine()) {
			packet.GameNr = Integer.parseInt(userInput.nextLine());
		}
		System.out.println("Team A: ");
		if (userInput.hasNextLine()) {
			packet.TeamA = userInput.nextLine();
		}
		System.out.println("Team B: ");
		if (userInput.hasNextLine()) {
			packet.TeamB = userInput.nextLine();
		}
		System.out.println("Location: ");
		if (userInput.hasNextLine()) {
			packet.Location = userInput.nextLine();
		}
		System.out.println("Competition: ");
		if (userInput.hasNextLine()) {
			packet.Competition = userInput.nextLine();
		}
		System.out.println("Date: ");
		if (userInput.hasNextLine()) {
			packet.Date = userInput.nextLine();
		}
		
		try {
			remoteObject.saveGD(packet);
		} catch (RemoteException e) { e.printStackTrace(); }
	}
    
    void saveQD() {
		
		QuarterDetails packet = new QuarterDetails (0,0,username,0,0);
		System.out.println("Enter the data below ");
		System.out.println("Game Number: ");
		if (userInput.hasNextLine()) {
			packet.GameNr = Integer.parseInt(userInput.nextLine());
		}
		System.out.println("Quarter no.: ");
		if (userInput.hasNextLine()) {
			packet.Quarter = userInput.nextLine();
		}
		System.out.println("Score Team A: ");
		if (userInput.hasNextLine()) {
			packet.ScoreTeamA = Integer.parseInt(userInput.nextLine());
		}
		System.out.println("Score Team B: ");
		if (userInput.hasNextLine()) {
			packet.ScoreTeamB = Integer.parseInt(userInput.nextLine());
		}
		
		try {
			remoteObject.saveQD(packet);
		} catch (RemoteException e) { e.printStackTrace(); }
	}
	
    void saveGS() {
		
		GameStats packet = new GameStats (0,0, username, username, username, username, username);
		System.out.println("Enter the data below ");
		System.out.println("Game Number: ");
		if (userInput.hasNextLine()) {
			packet.GameNr = Integer.parseInt(userInput.nextLine());
		}
		System.out.println("Winning Team: ");
		if (userInput.hasNextLine()) {
			packet.WhoWon = userInput.nextLine();
		}
		System.out.println("Final Score: ");
		if (userInput.hasNextLine()) {
			packet.FinalScore = userInput.nextLine();
		}
		System.out.println("Most Points in the game: ");
		if (userInput.hasNextLine()) {
			packet.MostPoints = userInput.nextLine();
		}
		System.out.println("Most Rebounds in the game: ");
		if (userInput.hasNextLine()) {
			packet.MostRebounds = userInput.nextLine();
		}
		System.out.println("Most Assists in the game: ");
		if (userInput.hasNextLine()) {
			packet.MostAssists = userInput.nextLine();
		}
		
		try {
			remoteObject.saveGS(packet);
		} catch (RemoteException e) { e.printStackTrace(); }
	}
	
    public static void saveFile(String string, String file) throws FileNotFoundException {
		
		PrintWriter saveFile = new PrintWriter(file);
		String nextLine = System.getProperty("line.separator");
		saveFile.println(string + nextLine);
		saveFile.close();
	}
	
	
	void showGD() throws FileNotFoundException {
		
		String line;
		String data = "";
		System.out.println("Enter number of the game: ");
		if (userInput.hasNextLine()) {
			line = userInput.nextLine();
			Vector<GameDetails> vec = null;
			try {
				vec = remoteObject.showGD(line);
			} catch (RemoteException e) { e.printStackTrace(); }
			System.out.println("There are " + vec.size() + " such game(s):");
			for (GameDetails s : vec)
				data = (data + " \n " + s + "\n********************");
			System.out.println(data);
			System.out.println("\nPress [1] to: Save the date in a file \nPress [2] to: Return");
			line = userInput.nextLine();
			if (line.matches("[1]")) {
				saveFile(data, "GameDetails.txt");
				System.out.println("GameDetails data is saved in a file.\n");
			}
			else if (line.matches("[2]")) {
				System.out.println("Returned to the menu.\n");
			}
			else {
				System.out.println("The command is invalid!\n");
			}
		}	
	}
	
	void showQD() throws FileNotFoundException {
		
		String line;
		String data = "";
		System.out.println("Enter number of the game: ");
		if (userInput.hasNextLine()) {
			line = userInput.nextLine();
			Vector<QuarterDetails> vec = null;
			try {
				vec = remoteObject.showQD(line);
			} catch (RemoteException e) { e.printStackTrace(); }
			System.out.println("There are " + vec.size() + " such game(s):");
			for (QuarterDetails s : vec)
				data = (data + " \n " + s + "\n********************");
			System.out.println(data);
			System.out.println("\nPress [1] to: Save the date in a file \nPress [2] to: Return");
			line = userInput.nextLine();
			if (line.matches("[1]")) {
				saveFile(data, "QuarterDetails.txt");
				System.out.println("QuarterDetails data is saved in a file.\n");
			}
			else if (line.matches("[2]")) {
				System.out.println("Returned to the menu.\n");
			}
			else {
				System.out.println("The command is invalid!\n");
			}
		}	
	}
	
	void showGS() throws FileNotFoundException {
		
		String line;
		String data = "";
		System.out.println("Enter number of the game: ");
		if (userInput.hasNextLine()) {
			line = userInput.nextLine();
			Vector<GameStats> vec = null;
			try {
				vec = remoteObject.showGS(line);
			} catch (RemoteException e) { e.printStackTrace(); }
			System.out.println("There are " + vec.size() + " such game(s):");
			for (GameStats s : vec)
				data = (data + " \n " + s + "\n********************");
			System.out.println(data);
			System.out.println("\nPress [1] to: Save the date in a file \nPress [2] to: Return");
			line = userInput.nextLine();
			if (line.matches("[1]")) {
				saveFile(data, "GameStats.txt");
				System.out.println("GameStats data is saved in a file.\n");
			}
			else if (line.matches("[2]")) {
				System.out.println("Returned to the menu.\n");
			}
			else {
				System.out.println("The command is invalid!\n");
			}
		}	
	}
	
	Object loop() throws FileNotFoundException {
		
		while(true) {
			String line;
			System.out.println( "\n*WELCOME IN THE ONLINE REPORTING BASKETBALL SYSTEM*"
					+           "\n**********<<<<Choose Your Preference!>>>>**********\n "
					+ "\n Press [1] to: Save Game Details       \n Press [2] to: Save Quarter Details"
					+ "\n Press [3] to: Save Game Stats         \n Press [4] to: Display Game Details"
					+ "\n Press [5] to: Display Quarter Details \n Press [6] to: Display Game Stats"
					+ "\n Press [7] to: Logout");
			if(userInput.hasNextLine()) {
				line = userInput.nextLine();
				if(!line.matches("[1234567]")) {
					System.out.println("Somethinh went wrong... Try one more time\n");
					continue;
				}
				switch (line) {
					case "1":
						saveGD();
						return loop();
					case "2":
						saveQD();
						return loop();
					case "3":
						saveGS();
						return loop();
					case "4":
						showGD();
						return loop();	
					case "5":
						showQD();
						return loop();
					case "6":
						showGS();
						return loop();
			
					case "7":
						return 0;
				}
			}
		}
	}
}