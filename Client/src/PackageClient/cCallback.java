package PackageClient;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import PackageInterfejsy.iCallback;

public class cCallback extends UnicastRemoteObject implements iCallback {
	
	public cCallback() throws RemoteException {
		super();
	}
	public void propagate(String nick, String text) throws RemoteException {
		System.out.println("Message received: " + text); 
	}
}