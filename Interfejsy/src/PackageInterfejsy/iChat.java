package PackageInterfejsy;

import java.rmi.*;
import java.util.Vector;
import instruction1.GameDetails;
import instruction1.GameStats;
import instruction1.QuarterDetails;


public interface iChat extends Remote {

   boolean register(String nick, iCallback n) throws RemoteException;
   boolean unregister(String nick) throws RemoteException;
  
   boolean saveGD(GameDetails packet) throws RemoteException;
   boolean saveGS(GameStats packet) throws RemoteException;
   boolean saveQD(QuarterDetails packet) throws RemoteException;
  
   Vector<String> mapGD(String device) throws RemoteException;
   Vector<String> mapQD(String device) throws RemoteException;
   Vector<String> mapGS(String device) throws RemoteException;
  
   Vector<GameDetails> showGD(String device) throws RemoteException;
   Vector<QuarterDetails> showQD(String device) throws RemoteException;
   Vector<GameStats> showGS(String device) throws RemoteException;
}
