package PackageInterfejsy;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface iCallback extends Remote {
	
	public void propagate(String nick, String text) throws RemoteException;
}

