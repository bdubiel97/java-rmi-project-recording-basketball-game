package instruction1;

public class GameDetails <K> extends GameNumber {
	public String TeamA;
	public String TeamB;
	public String Location;
	public String Competition;
	public String Date;
	

	public GameDetails(int xGameNr, int xBaffer,String xTeamA,String xTeamB, String xLocation, String xCompetition, String xDate) {
		
		super(xGameNr, xBaffer);
		
		TeamA = xTeamA;
		TeamB = xTeamB;
		Location = xLocation;
		Competition = xCompetition;
		Date = xDate;	
		
	}
	
	public String toString() {
		
		return "\nGAME DETAILS\n\nGame Number:" + GameNr +
			   "\nTeam A: " + TeamA +
			   "\nTeam B: " + TeamB +
			   "\nLocation: " + Location + 
			   "\nCompetition: " + Competition +
			   "\nDate: " + Date ;
		
	}

}
