package instruction1;

public class GameStats <K> extends GameNumber{
	public String WhoWon;
	public String FinalScore;
	public String MostPoints;
	public String MostRebounds;
	public String MostAssists;
	

	public GameStats(int xGameNr,int xBaffer, String xWhoWon, String xFinalScore, String xMostPoints, String xMostRebounds, String xMostAssists) {
		
		super(xGameNr, xBaffer);
		
		WhoWon = xWhoWon;
		FinalScore = xFinalScore;
		MostPoints = xMostPoints;
		MostRebounds = xMostRebounds;
		MostAssists = xMostAssists;
		

	}
	
	public String toString() {
		
		return 	   "\nGAME STATS\n\nGame Number: " + GameNr +
				   "\nWinning Team: " + WhoWon +
				   "\nFinal Score: " + FinalScore +
				   "\nMost points in the game: " + MostPoints + 
				   "\nMost rebound in the game: " + MostRebounds +
				   "\nMost assists in the game: " + MostAssists ;
		
	}

}
