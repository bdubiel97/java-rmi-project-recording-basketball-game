package instruction1;

public class QuarterDetails <K> extends GameNumber{
	public String Quarter;
	public int ScoreTeamA;
	public int ScoreTeamB;
	
	public QuarterDetails(int xGameNr,int xBaffer, String xQuarter, int xScoreteamA, int xScoreteamB) {
		
		super(xGameNr, xBaffer);
		
		Quarter = xQuarter;
		ScoreTeamA = xScoreteamA;
		ScoreTeamB = xScoreteamB;
		
	}
	
	public String toString() {
		
		return "\nQUARTER DETAILS\n\nGame Number: " + GameNr +
			   "\nQuarter number: " + Quarter +
			   "\nScoreTeamA: " + ScoreTeamA +
			   "\nScoreTeamB: " + ScoreTeamB ;	
		
	}
	

}
